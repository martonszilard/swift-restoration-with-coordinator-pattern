//
//  FirstViewController.swift
//  CoordinatorDemo
//
//  Created by Saad El Oulladi on 29/09/2018.
//  Copyright © 2018 touti. All rights reserved.
//

import UIKit

public protocol FirstViewControllerDelegate: class {
    func navigateToNextPage()
}

class FirstViewController: UIViewController {

    public weak var delegate: FirstViewControllerDelegate?
    
    let firstView = UIView()
    let button = UIButton(frame: CGRect(origin: CGPoint(x: 100, y: 100), size: CGSize(width: 200, height: 20)))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "FirstViewController"
        button.addTarget(self, action: #selector(goToSecondPageAction), for: .touchUpInside)
        button.setTitle("Go to second", for: .normal)
        button.setTitleColor(.blue, for: .normal)
                
        firstView.addSubview(button)
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: firstView.safeAreaLayoutGuide.topAnchor),
            button.leadingAnchor.constraint(equalTo: firstView.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: firstView.trailingAnchor)
        ])
        firstView.backgroundColor = .white
    }
    
    override func loadView() {
        view = firstView
    }
    
    @objc func goToSecondPageAction(_ sender: Any) {
        self.delegate?.navigateToNextPage()
    }
}

//
//  ThirdViewController.swift
//  CoordinatorDemo
//
//  Created by Saad El Oulladi on 29/09/2018.
//  Copyright © 2018 touti. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    public weak var delegate: SecondViewControllerDelegate?
    
    let thirdView = UIView()
    let button = UIButton(frame: CGRect(origin: CGPoint(x: 100, y: 100), size: CGSize(width: 200, height: 20)))
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "ThirdViewController"
        restorationIdentifier = "ThirdViewController"
        button.addTarget(self, action: #selector(navigateToFirstPageAction), for: .touchUpInside)
        button.setTitle("Go to first", for: .normal)
        button.setTitleColor(.blue, for: .normal)
                
        thirdView.addSubview(button)
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: thirdView.safeAreaLayoutGuide.topAnchor),
            button.leadingAnchor.constraint(equalTo: thirdView.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: thirdView.trailingAnchor)
        ])
        thirdView.backgroundColor = .white
    }
    
    public override func loadView() {
        view = thirdView
    }
    @objc func navigateToFirstPageAction(_ sender: Any) {
        self.delegate?.navigateToFirstPage()
    }
}

extension ThirdViewController {
    static func viewController(
        withRestorationIdentifierPath identifierComponents: [String],
        coder: NSCoder
    ) -> UIViewController? {
        return ThirdViewController()
    }
}

extension ThirdViewController {
    override func encodeRestorableState(with coder: NSCoder) {
        coder.encode("IDValue", forKey: "ID")
        print("BBB")
        super.encodeRestorableState(with: coder)
    }

    override func decodeRestorableState(with coder: NSCoder) {
        coder.decodeInteger(forKey: "ID")
        print("AAA", coder.decodeInteger(forKey: "ID"))
    }
}

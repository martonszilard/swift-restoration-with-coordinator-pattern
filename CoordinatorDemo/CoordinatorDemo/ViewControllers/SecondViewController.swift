//
//  SecondViewController.swift
//  CoordinatorDemo
//
//  Created by Saad El Oulladi on 29/09/2018.
//  Copyright © 2018 touti. All rights reserved.
//

import UIKit

public protocol SecondViewControllerDelegate: class {
    func navigateToFirstPage()
    func navigateToThirdPage()
}

class SecondViewController: UIViewController {
    
    public weak var delegate: SecondViewControllerDelegate?

    
    let secondView = UIView()
    let button = UIButton(frame: CGRect(origin: CGPoint(x: 100, y: 100), size: CGSize(width: 200, height: 20)))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "SecondViewController"
        button.addTarget(self, action: #selector(navigateToThirdPageAction), for: .touchUpInside)
        button.setTitle("Go to third", for: .normal)
        button.setTitleColor(.blue, for: .normal)
                
        secondView.addSubview(button)
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: secondView.safeAreaLayoutGuide.topAnchor),
            button.leadingAnchor.constraint(equalTo: secondView.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: secondView.trailingAnchor)
        ])
        secondView.backgroundColor = .white
    }
    
    override func loadView() {
        view = secondView
    }
    
    @objc func navigateBackToFirstpage() {
        self.delegate?.navigateToFirstPage()
    }
    
    @objc func navigateToThirdPageAction(_ sender: Any) {
        self.delegate?.navigateToThirdPage()
    }
}
